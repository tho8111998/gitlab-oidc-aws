## Requirements

| Name | Version |
|------|---------|
| terraform | ~> 1.0 |
| aws | ~> 4.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gitlab_url | The address of your GitLab instance, such as https://gitlab.com or http://gitlab.example.com. | `string` | `"https://gitlab.com"` | yes |
| audience | The address of your GitLab instance, such as https://gitlab.com or http://gitlab.example.com. | `string` | `"https://gitlab.com"` | yes |
| iam\_role\_policy\_arns | List of IAM policy ARNs to attach to the IAM role. | `list(string)` | `[]` | optional |
| create\_oidc\_provider | Flag to enable/disable the creation of the GitHub OIDC provider. | `bool` | `true` | yes |
| match\_field | Issuer, the domain of your GitLab instance. Change to sub if you want to use the filter to any project | `string` | aud | yes |
| match\_value | value | `list` | GitLab Instance URL | yes |

## Optional Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| attach\_admin\_policy | Flag to enable/disable the attachment of the AdministratorAccess policy. | `bool` | `false` | no |
| iam\_role\_name | Name of the IAM role to be created. This will be assumable by GitLab. | `string` | `"gitlab_action_role"` | no |
| iam\_role\_path | Path under which to create IAM role. | `string` | `"/"` | no |
| max\_session\_duration | Maximum session duration in seconds. | `number` | `3600` | no |

## Outputs

| Name | Description |
|------|-------------|
| iam\_role\_arn | ARN of the IAM role. |
